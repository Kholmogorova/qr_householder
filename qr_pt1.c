#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>
#include <pthread.h>
#define THREAD_NUM 4

typedef struct {
    int m, n;
    double **v;
} mat_t, mat;

typedef struct {
    mat a; 
    double *tau;
    double *u;
    int left;
    int right;
    int k;
} input;

mat matrix_new(int m, int n)
{
    int i;
    mat x;
    x.v = malloc(sizeof(*(x.v)) * m);
    x.v[0] = calloc(sizeof(*(x.v[0])), m * n);
    for (i = 0; i < m; i++){
        x.v[i] = x.v[0] + n * i;
    }
    x.m = m;
    x.n = n;
    return x;
}

void matrix_delete(mat m)
{
        free(m.v[0]);
        free(m.v);
}

/*c = a + b * s*/
void mv_add(double a[], double b[], double s,
                double c[], int n) {
    int i;
    for (i = 0; i < n; i++) {
        c[i] = a[i] + s * b[i];
    }
    return;
}


double norm(int dim, double *v) {
    int i;
    double res = 0;
    for (i = 0; i < dim; i++) {
        res += v[i] * v[i];
    }
    return sqrt(res);
}

double scal(int dim, double *v, double *u) {
    int i;
    double res = 0;
    for (i = 0; i < dim; i++) {
        res += v[i] * u[i];
    }
    return res;
}

/*y = x * mul*/
void vmul(double x[], double mul, double y[], int n) {
    int i;

    for (i = 0; i < n; i++) {
        y[i] = x[i] * mul;
    }
    return;
}

void matrix_show(mat m) {
    int i, j;

    for (i = 0; i < m.m; i++) {
        for (j = 0; j < m.n; j++) {
            printf("%8.3lf", m.v[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void *make_r_col(void *arg) {
    mat a;
    double *tau;
    double *u;
    int i, k, left, right;
    double aux;

    a = ((input *)arg)->a;
    tau = ((input *)arg)->tau;
    u = ((input *)arg)->u;
    left = ((input *)arg)->left;
    right = ((input *)arg)->right;
    k = ((input *)arg)->k;

    for (i = left; i < right; i++) {
        aux = scal(a.n - k, u, a.v[i] + k);
        mv_add(a.v[i] + k, u, -tau[k] * aux, a.v[i] + k, a.n - k);
    }

    return arg;
}

int householder(mat a, double tau[], double u[])
{
    pthread_t threads[THREAD_NUM];
    int i, k, step;
    double aux, norm_a;
    input arg;

    u[0] = 1;
    for (k = 0; k < a.m && k < a.n; k++) {
        norm_a = norm(a.n - k, a.v[k] + k);
        if (a.v[k][k] < 0){
            norm_a = -norm_a;
        }
        aux = a.v[k][k] + norm_a;
        vmul(a.v[k] + k + 1, 1 / aux, a.v[k] + k + 1, a.n - k - 1);
        for (i = 1; i < a.n - k; i++) {
            u[i] = a.v[k][k + i];
        }
        aux = scal(a.n - k, u, u); 
        tau[k] = 2 / aux;
        a.v[k][k] = -norm_a;
        arg.a = a;
        arg.u = u;
        arg.tau = tau;
        arg.right = k + 1;
        arg.k = k;
        if (THREAD_NUM != 1) {
            step = (a.m - k - 1) / (THREAD_NUM - 1);
        } else {
            step = a.m - k - 1;
        }
        if  (k > 0) {
            for (i = 1; i < THREAD_NUM; i++) {
                if (pthread_join(threads[i], NULL) != 0) {
                    return 1;
                }
            }
        }
        for (i = 0; i < THREAD_NUM; i++) {
            arg.left = arg.right;
            arg.right += step;
            if (i == THREAD_NUM - 1) {
                arg.right = a.m;
            }
            if (pthread_create(&threads[i], NULL, 
                    make_r_col, (void *)&arg) != 0)
            {
                return 1;
            }
            usleep(2);
        }
        if (pthread_join(threads[0], NULL) != 0) {
            return 1;
        }
    }
    for (i = 1; i < THREAD_NUM; i++) {
        if (pthread_join(threads[i], NULL) != 0) {
            return 1;
        }
    }
    return 0;
}


int main(int argc, char *argv[]) {
    int dim, i, j;
    double time;
    mat r;
    double *aux;
    double *tau; 
    
    if (!scanf("%d", &dim)) {
        return 0;
    }

    r = matrix_new(dim, dim);
    tau = malloc(sizeof(*tau) * dim);
    aux = malloc(sizeof(*aux) * dim);
    for (i = 0; i < r.m; i++) {
        for (j = 0; j < r.n; j++) {
            scanf("%lf", r.v[i] + j);
        }
    }

    time = omp_get_wtime();
    //basic cycle
    if (householder(r, tau, aux)) {
        return 1;
    }
    printf("time: %.2lfsec.\n", omp_get_wtime() - time);

//    matrix_show(r);
    matrix_delete(r);
    free(tau);
    free(aux);
    return 0;
}
