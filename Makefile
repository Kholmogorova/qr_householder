CC=gcc

CFLAGS=-c -Wall -O0 -g

LFLAGS=-llapacke -lm -g  

all: factorization.exe
.PHONY: all

factorization_omp.exe: qr_openmp.o
	$(CC) -fopenmp  $< -o $@ $(LFLAGS)

qr_openmp.o: qr_openmp.c
	$(CC) -fopenmp $(CFLAGS) $< -o $@ 

factorization_pt.exe: qr_pt.o
	$(CC) -fopenmp -pthread $< -o $@ $(LFLAGS)

qr_pt.o: qr_pt.c
	$(CC) -fopenmp -pthread $(CFLAGS) $< -o $@ 

factorization_pt1.exe: qr_pt1.o
	$(CC) -fopenmp -pthread $< -o $@ $(LFLAGS)

qr_pt1.o: qr_pt1.c
	$(CC) -fopenmp -pthread $(CFLAGS) $< -o $@ 

create.exe: create.o
	$(CC) $< -o $@ $(LFLAGS)
    
create.o: create.c
	$(CC) $(CFLAGS) $< -o $@ 

matrix: create.exe
	./create.exe 4096 50 3 >matrix.txt

factorization: factorization.exe
	./factorization.exe <matrix.txt

clean:
	rm *.o *.exe
