#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <omp.h>

typedef struct {
    int m, n;
    double **v;
} mat_t, mat;

mat matrix_new(int m, int n)
{
    int i;
    mat x;
    x.v = malloc(sizeof(*(x.v)) * m);
    x.v[0] = calloc(sizeof(*(x.v[0])), m * n);
    #pragma omp parallel for
    for (i = 0; i < m; i++){
        x.v[i] = x.v[0] + n * i;
    }
    x.m = m;
    x.n = n;
    return x;
}

void matrix_delete(mat m)
{
        free(m.v[0]);
        free(m.v);
}

/*c = a + b * s*/
void mv_add(double a[], double b[], double s,
                double c[], int n) {
    int i;

    #pragma omp parallel for
    for (i = 0; i < n; i++) {
        c[i] = a[i] + s * b[i];
    }
    return;
}


double norm(int dim, double *v) {
    int i;
    double res = 0;

    #pragma omp parallel for reduction(+:res)
    for (i = 0; i < dim; i++) {
        res += v[i] * v[i];
    }
    return sqrt(res);
}

double scal(int dim, double *v, double *u) {
    int i;
    double res = 0;
    #pragma omp parallel for reduction(+:res)
    for (i = 0; i < dim; i++) {
        res += v[i] * u[i];
    }
    return res;
}

/*y = x * mul*/
void vmul(double x[], double mul, double y[], int n) {
    int i;
    #pragma omp parallel for
    for (i = 0; i < n; i++) {
        y[i] = x[i] * mul;
    }
    return;
}

void matrix_show(mat m) {
    int i, j;

    for (i = 0; i < m.m; i++) {
        for (j = 0; j < m.n; j++) {
            printf("%8.3lf", m.v[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void householder(mat a, double tau[], double u[])
{
    int i,k;
    double aux, norm_a;

    u[0] = 1;
    for (k = 0; k < a.m && k < a.n; k++) {
        norm_a = norm(a.n - k, a.v[k] + k);
        if (a.v[k][k] < 0){
            norm_a = -norm_a;
        }
        aux = a.v[k][k] + norm_a;
        vmul(a.v[k] + k + 1, 1 / aux, a.v[k] + k + 1, a.n - k - 1);
        #pragma omp parallel for
        for (i = 1; i < a.n - k; i++) {
            u[i] = a.v[k][k + i];
        }
        aux = scal(a.n - k, u, u); 
        tau[k] = 2 / aux;
        a.v[k][k] = -norm_a;
        #pragma omp parallel for schedule(guided) private(aux)
        for (i = k + 1; i < a.m; i++) {
            aux = scal(a.n - k, u, a.v[i] + k);
            mv_add(a.v[i] + k, u, -tau[k] * aux, a.v[i] + k, a.n - k);
        }
    }
    return;
}


int main(int argc, char *argv[]) {
    int dim, i, j;
    double time;
    mat r;
    double *aux;
    double *tau; 
    
    if (!scanf("%d", &dim)) {
        return 0;
    }

    r = matrix_new(dim, dim);
    tau = malloc(sizeof(*tau) * dim);
    aux = malloc(sizeof(*aux) * dim);
    for (i = 0; i < r.m; i++) {
        for (j = 0; j < r.n; j++) {
            scanf("%lf", r.v[i] + j);
        }
    }

    time = omp_get_wtime();
//    omp_set_dynamic(1);
//    omp_set_nested(0);
    //basic cycle
    householder(r, tau, aux);
    printf("time: %.2lfsec.\n", omp_get_wtime() - time);

//    matrix_show(r);
    matrix_delete(r);
    free(tau);
    free(aux);
    return 0;
}
