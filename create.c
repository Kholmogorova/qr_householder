#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    int num, lim, i, j, seed;
    if (argc != 4) {
        printf("Error.");
        return 0;
    }
    num = atoi(argv[1]);/*size of matrix*/
    lim = atoi(argv[2]);/*maximal element module*/
    seed = atoi(argv[3]);/*seed for rand()*/
    srand(seed);
    lim = lim / 2;
    printf("%d\n", num);
    for (i = 0; i < num; i++) {
        for (j = 0; j < num; j++) {
            printf("%.5f ", ((float)(rand() - rand())) / RAND_MAX * lim);
        }
        printf("\n");
    }
    return 0;
}
